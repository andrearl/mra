package tdd.training.mra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarsRover {

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	private boolean [][] gridPlanet;
	private int[] roverPosition = new int[2];
	private String direction;
	private String[] DIR= {"W", "N", "E", "S"};
	private int planetX;
	private int planetY;
	private String obstaclesFind = "";
	List<String> obstaclesMet = new ArrayList<>();
	
	
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.gridPlanet = new boolean[planetX][planetY];
		this.roverPosition[0] = 0;
		this.roverPosition[1] = 0;
		this.direction = "N";
		this.planetX = planetX;
		this.planetY = planetY;
		setStartGrid();
		signObstacleOnThePlanet(planetObstacles);
	}
	
	private void setStartGrid() {
		for(boolean[] line : gridPlanet) {
			Arrays.fill(line, Boolean.FALSE);
		}
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	
	private void signObstacleOnThePlanet(List<String> planetObstacles) {
		for( int i = 0; i < planetObstacles.size(); i++) {
			String[] obstacles = planetObstacles.get(i).split(",");
			gridPlanet[Integer.parseInt(obstacles[0])][Integer.parseInt(obstacles[1])] = true;
		}
	}
	
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		return gridPlanet[x][y];
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		for(int i = 0; i < commandString.length(); i++) {
			char command = commandString.charAt(i);
			if(command == 'f') {
				if(setMoveAxis()) {
					roverPosition[0] = roverPosition[0] + 1;
					roverPosition[0] = beyondEdge(roverPosition[0], planetX);
					if(gridPlanet[roverPosition[0]][roverPosition[1]]) {
						setObstacle();
						roverPosition[0] = roverPosition[0] - 1;
					}
				} else {
					roverPosition[1] = roverPosition[1] + 1;
					roverPosition[1] = beyondEdge(roverPosition[1], planetY);
					if(gridPlanet[roverPosition[0]][roverPosition[1]]) {
						setObstacle();
						roverPosition[1] = roverPosition[1] - 1;
					}
					
				}
			}else if (command == 'r' || command == 'l') {
				setDirection(command);
			} else if(command == 'b') {
				if(setMoveAxis()) {
					roverPosition[0] = roverPosition[0] - 1;
					roverPosition[0] = beyondEdge(roverPosition[0], planetX);
					if(gridPlanet[roverPosition[0]][roverPosition[1]]) {
						setObstacle();
						roverPosition[0] = roverPosition[0] + 1;
					}
				} else {
						roverPosition[1] = roverPosition[1] - 1;
						roverPosition[1] = beyondEdge(roverPosition[1], planetY);
						if(gridPlanet[roverPosition[0]][roverPosition[1]]) {
							setObstacle();
							roverPosition[1] = roverPosition[1] + 1;
					}
				}
			}
		}
		return "(" + roverPosition[0] +"," +roverPosition[1] +"," + direction + ")" +obstaclesFind;
	}
	
	
	private boolean setObstacle() {
		if(gridPlanet[roverPosition[0]][roverPosition[1]]) {
			boolean check = true;
			for(int i = 0; i < obstaclesMet.size(); i++) {
				String [] obstacle = obstaclesMet.get(i).split(","); 
				if(Integer.parseInt(obstacle[0]) == roverPosition[0] && Integer.parseInt(obstacle[1]) == roverPosition[1]) {
					return false;
				} 
			}
			if(check == true) {
				obstaclesMet.add(roverPosition[0]+","+roverPosition[1]);
				obstaclesFind +="(" + roverPosition[0] + "," + roverPosition[1] + ")";
			}
		}
		return false;
	}
	
	private boolean setMoveAxis() {
		if(direction.equals(DIR[0]) || direction.equals(DIR[2])) {
			return true;
		}
		return false;
	}
	
	private int beyondEdge(int position, int control) {
		if(position > control-1) {
			position = 0;
		} else if( position < 0) {
			position = control-1;
		}
		return position;
	}
	
	private void setDirection(char command) {
		int position = 0; 
		for(int i = 0; i < DIR.length; i++) {
			if(DIR[i].equals(direction)) {
				position = i;
			}
		}
		if(command == 'r') {
			if(position < DIR.length-1) {
				direction = DIR[position + 1];
			} else {
				direction = DIR[0];
			}
		} else {
			if(position == 0) {
				direction = DIR[3];
			} else {
				direction = DIR[position -1];
			}
		}
	}

}
