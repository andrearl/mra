package tdd.training.mra;

import static org.junit.Assert.*;

import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import tdd.training.mra.*;

public class MarsRoverTest {

	@Test
	public void testPlanetShouldNotHaveObstacle() throws Exception{
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("4,7");
		planetObstacles.add("2,3");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertTrue(rover.planetContainsObstacleAt(4,7));
	}
	
	@Test
	public void testRoverShouldBeAtStart() throws Exception{
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,N)",rover.executeCommand(" "));
	}

	@Test
	public void testRoverShouldTurningRight() throws Exception{
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,E)",rover.executeCommand("r"));
	}
	
	@Test
	public void testRoverShouldTurningLeft() throws Exception{
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,W)",rover.executeCommand("l"));
	}
	
	@Test
	public void testRoverShouldMoveForward() throws Exception{
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,1,N)",rover.executeCommand("f"));
	}
	
	@Test
	public void testRoverShouldMoveBackward() throws Exception{
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("r");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		assertEquals("(2,3,E)",rover.executeCommand("b"));
	}
	
	@Test
	public void testRoverShouldMoveWithCombination() throws Exception{
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(2,2,E)",rover.executeCommand("ffrff"));
	}
	
	@Test
	public void testRoverShouldReturnToTheOppositeEdge() throws Exception{
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,9,N)",rover.executeCommand("b"));
	}
	
	@Test
	public void testRoverShouldBeStuckByObastacle() throws Exception{
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("2,2");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(1,2,E)(2,2)",rover.executeCommand("ffrfff"));
	}
}
